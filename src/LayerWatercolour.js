import React, { useState, useEffect } from "react";
import TileLayer from "ol/layer/Tile";
import XYZ from "ol/source/XYZ";
// import MapContext from "./MapContext";

function LayerWaterColour({ map, opacity }) {
  //   const { map } = useContext(MapContext);

  useEffect(() => {
    if (!map) return;

    let watercolourView = new TileLayer({
      source: new XYZ({
        url: "https://stamen-tiles.a.ssl.fastly.net/watercolor/{z}/{x}/{y}.jpg",
        attributions:
          'Map tiles by <a href="http://stamen.com">Stamen Design</a>, under <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a>. Data by <a href="http://openstreetmap.org">OpenStreetMap</a>, under <a href="http://www.openstreetmap.org/copyright">ODbL</a>.',
      }),
      title: "watercolour",
      visible: true,
    });

    map.addLayer(watercolourView);
    watercolourView.setOpacity(opacity);

    return () => {
      if (map) {
        map.removeLayer(watercolourView);
      }
    };
  }, [map, opacity]);
  return null;
}

export default LayerWaterColour;
