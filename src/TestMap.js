import React, { useState, useRef, useEffect } from "react";
import "ol/ol.css";
import Map from "ol/Map";
import View from "ol/View";
import TileLayer from "ol/layer/Tile";
import Button from "./Button";
import AddDrawing from "./AddDrawing";
import LayerSwitch from "./LayerSwitch";
import Pointer from "./Pointer";
import Context from "./Context";
import NewLayerSwitch from "./NewLayerSwitch";
import OSMSource from "ol/source/OSM";

function TestMap() {
  const [map, setMap] = useState();

  const mapRef = useRef();
  mapRef.current = map;

  function getMap() {
    const initialMap = new Map({
      view: new View({ center: [0, 0], zoom: 3 }),
      layers: [standard],
      target: mapRef.current,
      controls: [],
    });
    setMap(initialMap);
  }

  useEffect(() => {
    getMap();
  }, []);

  const standard = new TileLayer({
    source: new OSMSource(),
    visible: true,
    opacity: 1,
  });

  return (
    <Context.Provider value={{ map }}>
      <div ref={mapRef} className="map" />
      <div>
        <Button />
        <AddDrawing />
        <Pointer />
        {/* <LayerSwitch /> */}
        <NewLayerSwitch />
      </div>
    </Context.Provider>
  );
}

export default TestMap;
