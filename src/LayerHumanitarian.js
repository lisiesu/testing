import React, { useState, useEffect } from "react";
import TileLayer from "ol/layer/Tile";
import OSMSource from "ol/source/OSM";
// import layers from "./AllLayers";

function LayerHumanitarian({ map, opacity }) {
  useEffect(() => {
    if (!map) return;

    let humanitarianView = new TileLayer({
      source: new OSMSource({
        url: "https://{a-c}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png",
        title: "humanitarian",
        visible: true,
      }),
    });

    map.addLayer(humanitarianView);
    humanitarianView.setOpacity(opacity);
    // console.log(layers[0].id)

    return () => {
      if (map) {
        map.removeLayer(humanitarianView);
      }
    };
  }, [map, opacity]);
  return null;
}

export default LayerHumanitarian;
