import React, { useState, useRef, useContext } from "react";
import LayerHumanitarian from "./LayerHumanitarian";
import LayerWaterColour from "./LayerWatercolour";
import Context from "./Context";


function LayerSwitch() {

  const { map } = useContext(Context);
  
  const [layer1, setLayer1] = useState(false);
  const [layer2, setLayer2] = useState(false);
  const [opacity1, setOpacity1] = useState(1);
  const [opacity2, setOpacity2] = useState(1);

  const opacity1Ref = useRef();
  const opacity2Ref = useRef();

  function toggleLayer1(event) {
    setLayer1(event.target.checked);
  }

  function toggleLayer2(event) {
    setLayer2(event.target.checked);
  }

  function changeOpacity1() {
    let newOpacity = parseFloat(opacity1Ref.current.value);
    setOpacity1(newOpacity);
    console.log(newOpacity);
  }

  function changeOpacity2() {
    let newOpacity = parseFloat(opacity2Ref.current.value);
    setOpacity2(newOpacity);
    console.log(newOpacity);
  }

  return (
    <div>
      {layer1 && <LayerHumanitarian map={map} opacity={opacity1} />}

      {layer2 && <LayerWaterColour map={map} opacity={opacity2} />}
      <div>
        <div>
          Humanitarian
          <input
            type="checkbox"
            checked={layer1}
            onChange={toggleLayer1}
          />
          <label>
            {" "}
            Opacity{" "}
            <input
              ref={opacity1Ref}
              type="range"
              min="0"
              max="1"
              step="0.01"
              onChange={changeOpacity1}
            />
            <span>{opacity1}</span>
          </label>
        </div>
        <div>
          Watercolour
          <input type="checkbox" checked={layer2} onChange={toggleLayer2} />
          <label>
            {" "}
            Opacity{" "}
            <input
              ref={opacity2Ref}
              type="range"
              min="0"
              max="1"
              step="0.01"
              onChange={changeOpacity2}
            />
            <span>{opacity2}</span>
          </label>
        </div>
      </div>
    </div>
  );
}

export default LayerSwitch;
